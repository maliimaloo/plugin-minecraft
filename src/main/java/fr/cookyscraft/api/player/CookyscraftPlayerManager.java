package fr.cookyscraft.api.player;

import org.bukkit.entity.Player;

import java.util.UUID;

public abstract interface CookyscraftPlayerManager
{
    public abstract CookyscraftPlayer getPlayer(Player paramPlayer);

    public abstract CookyscraftPlayer getPlayer(UUID paramUUID);

    public abstract CookyscraftPlayer getPlayer(String paramString);
}
