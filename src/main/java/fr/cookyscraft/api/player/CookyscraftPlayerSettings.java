package fr.cookyscraft.api.player;

public abstract interface CookyscraftPlayerSettings {
    public abstract boolean isVanished();

    public abstract void setVanished(boolean paramBoolean);
}
