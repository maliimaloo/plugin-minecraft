package fr.cookyscraft.api.player;

import fr.cookyscraft.api.api.CookyscraftAPI;
import org.bukkit.entity.Player;

public abstract interface CookyscraftPlayer
{
    public static CookyscraftPlayer getPlayer(Player player)
    {
        return CookyscraftAPI.getPlayerManager().getPlayer(player);
    }

    public abstract CookyscraftPlayerSettings getSettings();
}
