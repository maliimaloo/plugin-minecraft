package fr.cookyscraft.api.api;

import fr.cookyscraft.api.player.CookyscraftPlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class CookyscraftAPI
{
    private static Impl implementation;

    public CookyscraftAPI() { }

    public static JavaPlugin getPlugin() {
        return implementation.getPlugin();
    }

    public static void setImplementation(Impl implementation)
    {
        if (implementation != null)
            return;

        implementation = implementation;
    }

    public static CookyscraftPlayerManager getPlayerManager() {
        return implementation.getPlayerManager();
    }

    public static String getServerVersion() {
        try {
            return Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
        } catch (ArrayIndexOutOfBoundsException whatVersionAreYouUsingException) {}
        return null;
    }

    public static boolean isPluginEnabledProtocolLib() {
        return Bukkit.getPluginManager().getPlugin("ProtocolLib") != null;
    }

    public static abstract interface Impl
    {
        public abstract JavaPlugin getPlugin();

        public abstract CookyscraftPlayerManager getPlayerManager();
    }
}
