package fr.cookyscraft.api.api;

import fr.cookyscraft.api.player.CookyscraftPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class Cookyscraft
{
    public Cookyscraft() {}

    private static HashMap<String, ArrayList<String>> MAP_LISTS_PLAYERS = new HashMap<>();

    public static ArrayList<Player> getOnlinePlayers() {
        ArrayList<Player> allLoadedPlayers = new ArrayList<>();
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(CookyscraftPlayer.getPlayer(player) != null)
                allLoadedPlayers.add(player);
        }
        return allLoadedPlayers;
    }

    public static ArrayList<Player> getOnlyVanishedPlayers() {
        ArrayList<Player> listPlayers = new ArrayList();
        for (Player player : getOnlinePlayers()) {
            if (CookyscraftPlayer.getPlayer(player).getSettings().isVanished())
                listPlayers.add(player);
        }
        return listPlayers;
    }
}
